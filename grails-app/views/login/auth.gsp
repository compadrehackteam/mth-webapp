<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page | Measuring the hive</title>

    <!-- CORE CSS-->
    <asset:stylesheet src="materialize.css"/>
    <asset:stylesheet src="style.css"/>
    <!-- Custome CSS-->
    <asset:stylesheet src="page-center.css"/>

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <asset:stylesheet src="prism.css"/>
    <asset:stylesheet src="perfect-scrollbar.css"/>


</head>

<body class="cyan loaded">

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>

    <asset:image src="background.png"/>

    <div class="loader-section section-left"></div>

    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<div id="login-page" class="row">
    %{--<div id="infoDiv" class="alert alert-warning alert-dismissable">--}%
        %{--<strong>Info!</strong> This alert box could indicate a--}%
    %{--</div>--}%
    <div class="col s12 z-depth-4 card-panel">

        <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" class="form-signin"
              autocomplete="off">

            <div class="row">
                <div class="input-field col s12 center">
                    <asset:image src="cht.png" alt="" class="circle responsive-img valign profile-image-login"/>
                    <p class="center login-form-text">Measuring The Hive</p>
                </div>
            </div>

            <div class="row margin">
                <div class="input-field col s12">
                    %{--<label for="username" class="control-label"><g:message--}%
                            %{--code='springSecurity.login.username.label'/>:</label>--}%
                    <input type="text" value="demo" class="form-control" name="${usernameParameter ?: 'username'}"
                           id="username"/>
                </div>
            </div>

            <div class="row margin">
                <div class="input-field col s12">
                    %{--<label for="password"><g:message code='springSecurity.login.password.label'/>:</label>--}%
                    <input type="password" value="demo" class="form-control text_"
                           name="${passwordParameter ?: 'password'}"
                           id="password"/>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m12 l12  login-text">
                    <input id="remember-me" type="checkbox">
                    <label for="remember-me">Remember me</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <div class="form-group">
                        <input class="btn waves-effect waves-light col s12" type="submit" id="submit"
                               value="${message(code: 'springSecurity.login.button')}"/>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>



<!-- ================================================
    Scripts
    ================================================ -->

<!-- jQuery Library -->
<asset:javascript src="jquery-1.js"/>
<asset:javascript src="bootstrap/dist/js/bootstrap.min.js"/>

<!--materialize js-->
<asset:javascript src="materialize.js"/>
<!--prism-->
<asset:javascript src="prism.js"/>
<!--scrollbar-->
<asset:javascript src="perfect-scrollbar.js"/>
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<asset:javascript src="plugins.js"/>

<div class="hiddendiv common"></div></body></html>