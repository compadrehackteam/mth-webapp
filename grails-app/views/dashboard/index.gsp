<!DOCTYPE html>
<html>
<head>

    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'dashboard.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- Bootstrap -->
    <asset:stylesheet src="bootstrap/bootstrap.css"/>
    <!-- styles -->


    <asset:stylesheet src="styles.css"/>


    <asset:stylesheet src="dashboard/dashboard.css"/>

</head>

<body>
<asset:javascript src="jquery-2.2.0.min.js"/>
<asset:stylesheet src="datepicker/css/mydatepicker.css"/>

<asset:stylesheet src="bootstrap/bootstrap-tour-standalone.min.css"/>
<asset:javascript src="bootstrap/bootstrap-tour-standalone.js"/>
<asset:javascript src="spin.js"/>

<asset:javascript src="chart/Chart.min.js"/>

<asset:javascript src="moment/moment.min.js"/>

<asset:javascript src="dashboard/general.js"/>

<script>

    var people = new Array();
    <g:each in="${data}">
    var fhour = "${it.fhour}";
    var clients = ${it.clients};
    var packet = {"fhour": fhour, "clients": clients};
    people.push(packet);
    </g:each>

</script>

<div class="page-content" id="pageContent" >
    %{--<h3 id="title" data-width="fit" style="margin-top: 40px">Histórico</h3>--}%

    <div class="row">

        <div class="container">
            <!-- Modal -->
            <div class="modal fade" id="modalTour" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" id="modalHeader" style="background-color: #2e6da4">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color: white"><strong>La prueba en un entorno real</strong>
                            </h4>
                        </div>

                        <div class="modal-body" id="modalBody">
                            <img id="modalImage" src="https://s3-eu-west-1.amazonaws.com/compadrebucket/womad1.png"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="orphan">
        <div class="col-md-9" style="padding-bottom: 12px">

            <section id="chartSection">
                <div id="chartSectionDiv" class="col-md-12 row container" style=" background-color: whitesmoke;">
                    <div id="chartDivBody" class="panel-body" style="padding-right: 20px;">
                        <canvas style="width: 100%" id="lineChart"></canvas>
                    </div>
                </div>
                %{--</div>--}%
            </section>
        </div>

        <div id="chartTemplate" class="chartTemplate">

            <g:render template="chartTemplate" model="model"/>
        </div>

        <div class="col-md-3">
            <div id="controlPanel" class="panel panel-default" style="margin-top: 50px;">
                <div class="panel-heading" style="background-color: #2e6da4; height: 40px" align="center"><p
                        style="color: white"><strong>Dispositivo</strong></p></div>

                %{--<h3 align="center">Dispositivo</h3>--}%

                <div class="row" style="padding-bottom: 10px; padding-top: 10px">
                    <div class="col-md-12">

                        <div id="clientSelectorDiv" align="center">
                            <select class="selectpicker" data-width="fit" onchange="clientPickerChanged()"
                                    data-live-search="true" id="clientSelector">
                                <g:each in="${clients}">
                                    <option id=${it.clientId} value= ${it.clientId}>${it.clientId}</option>
                                </g:each>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="panel-heading" style="background-color: #2e6da4; height: 40px" align="center"><p
                        style="color: white"><strong>Fecha</strong></p></div>

                %{--<div class="panel-heading" align="center">Fecha</div>--}%

                <div class="row" style="padding-bottom: 10px; padding-top: 10px" id='myDatePickerRow'>
                    <div class="col-md-12" style="position:relative">
                        <div class="container" style="position: relative" align="center">
                            <input id='myDatePicker' value="05/09/2017" onchange="datePickerChanged()"
                                   data-provide='datepicker' style="background-color: #ffffff"/>
                        </div>
                    </div>
                </div>

                <div class="panel-heading" style="background-color: #2e6da4; height: 40px" align="center"><p
                        style="color: white"><strong>Métricas</strong></p></div>
                %{--<div id="metricRow" class="panel-heading" align="center"></div>--}%

                <div id="metricsMeasures">
                    <div class="row" style="padding-bottom: 10px; padding-top: 10px">

                        <div class="col-md-6" style="position:relative">
                            <div class="checkbox" style="padding-left: 10px">
                                <label><input type="checkbox" id="humidityCheckbox"
                                              onclick="checkboxClicked('humidity')"
                                              value="">Humedad</label>
                            </div>
                        </div>

                        <div class="col-md-6" style="position:relative">
                            <div class="checkbox" style="padding-right: 10px">
                                <label><input type="checkbox" id="temperatureCheckbox"
                                              onclick="checkboxClicked('temperature')"
                                              value="">Temperatura</label>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 10px; padding-top: 10px">
                        <div class="col-md-6" style="position:relative">
                            <div class="checkbox" style="padding-left: 10px">
                                <label><input type="checkbox" id="concertsCheckbox"
                                              onclick="checkboxClicked('concerts')"
                                              value="">Conciertos</label>
                            </div>
                        </div>

                        <div class="col-md-6" style="position:relative">
                            <div class="checkbox" style="padding-right: 10px">
                                <label><input type="checkbox" id="rainCheckbox" onclick="checkboxClicked('rain')"
                                              value="">Lluvia</label>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 10px; padding-top: 10px">
                        <div class="col-md-12" style="position:relative" align="center">
                            <div id="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <asset:javascript src="api/APIClient.js"/>
    <asset:javascript src="dashboard/dashboard-controller.js"/>
    %{--<asset:javascript src="dashboard/tour.js"/>--}%


    <asset:javascript src="bootstrap/js/transition.js"/>
    <asset:javascript src="bootstrap/js/collapse.js"/>
    <asset:javascript src="js/bootstrap.min.js"/>
    <asset:javascript src="bootstrap-datetimepicker.min.js"/>
    <asset:javascript src="jquery-ui-1.12.1/jquery-ui.js"/>
    <asset:stylesheet src="bootstrap-select.min.css"/>
    <asset:javascript src="bootstrap-select.min.js"/>


    <asset:javascript src="dashboard/dashboard.js"/>

    <asset:javascript src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"/>
    <asset:javascript src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"/>
</body>

</html>