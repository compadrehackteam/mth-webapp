<asset:javascript src="dashboard/chart.js"/>

<script>
    var people = new Array();
    <g:each in="${data}">
    var fhour = "${it.fhour}";
    var clients = ${it.clients};
    var packet = {"fhour" : fhour , "clients" : clients};
    people.push(packet);
    </g:each>

    var temperatures = new Array();
    <g:each in="${dataTemperature}">
    var fhour = "${it.fhour}";
    var temp = ${it.value};
    var packet = {"fhour" : fhour , "temp" : temp};
    temperatures.push(packet);
    </g:each>

    var concerts = new Array();
    <g:each in="${dataConcerts}">
    var fhour = "${it.fhour}";
    var concert = ${it.concert};
    var packet = {"fhour" : fhour , "concert" : concert};
    concerts.push(packet);
    </g:each>

    var humidity = new Array();
    <g:each in="${dataHumidity}">
    var fhour = "${it.fhour}";
    var humidityPacket = ${it.value};
    var packet = {"fhour" : fhour , "humidity" : humidityPacket};
    humidity.push(packet);
    </g:each>

    var rain = new Array();
    <g:each in="${dataRain}">
    var fhour = "${it.fhour}";
    var rainPacket = ${it.value};
    var packet = {"fhour" : fhour , "rain" : rainPacket};
    rain.push(packet);
    </g:each>

    inflateBarCharView(people);
    reloadChart(barChart);

</script>

