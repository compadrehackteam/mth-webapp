<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <asset:stylesheet src="bootstrap/dist/css/bootstrap.min.css"/>
    <!-- NProgress -->
    <asset:stylesheet src="nprogress/nprogress.css"/>
    <!-- iCheck -->
    <asset:stylesheet src="iCheck/skins/flat/green.css"/>
    <asset:stylesheet href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <!-- bootstrap-progressbar -->
    <asset:stylesheet src="bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"/>
    <!-- JQVMap -->
    <asset:stylesheet src="jqvmap/dist/jqvmap.min.css"/>
    <!-- bootstrap-daterangepicker -->
    <asset:stylesheet src="bootstrap-daterangepicker/daterangepicker.css"/>
    <!-- Custom Theme Style -->
    <asset:stylesheet src="custom.min.css"/>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="https://image.flaticon.com/icons/svg/61/61205.svg" alt="">demo
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                %{--<li><a href="javascript:;"> Profile</a></li>--}%
                                %{--<li><a href="javascript:;"> Settings</a></li>--}%
                                <li><g:link uri="/logout" absolute="true">Cerrar sesión</g:link></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/webapp/dashboard" class="site_title">
                        <img id="logoCompadre" height="42" width="42" src="${resource(dir: 'images', file: 'cht.png')}"
                             alt=""/>
                        <span>Dashboard</span>
                    </a>
                </div>

                <div class="clearfix"></div>
                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        %{--<h3>General</h3>--}%
                        <ul class="nav side-menu options">
                            <li id="menuHome" onclick="menuHomeClicked()"><g:link controller="dashboard"
                                                                                  action="index"><img height="20"
                                                                                                      width="20"
                                                                                                      src="https://s3-eu-west-1.amazonaws.com/compadrebucket/chart.png"> Histórico</g:link>
                            </li>
                            <li id="menuMap" onclick="menuMapClicked()"><g:link controller="map" action="index"><img
                                    height="20" width="20"
                                    src="https://s3-eu-west-1.amazonaws.com/compadrebucket/globe.png"> Sobre el mapa</g:link>
                            </li>
                            %{--<li><g:link uri="/logout" absolute="true"><img height="20" width="20" src="https://s3-eu-west-1.amazonaws.com/compadrebucket/logout.png"> Cerrar sesión</g:link></li>--}%
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- page content -->
        <div class="right_col" style="height: 100vh;" role="main">
            <div class="container">
                <!-- Modal -->
                <div class="modal fade" id="modalInfo" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header" id="modalInfoHeader" style="background-color: #2e6da4">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" style="color: white"><strong>Información</strong></h4>
                            </div>

                            <div class="modal-body" id="modalInfoBody">

                                <ul class="nav nav-pills">
                                    <li class="active" onclick="infoTab1Clicked()"><a href="#instantTab"
                                                                                      data-toggle="tab">Sobre nosotros</a>
                                    </li>
                                    <li onclick="infoTab2Clicked()"><a href="#historicTab"
                                                                       data-toggle="tab">Aplicaciones</a></li>
                                    <li onclick="infoTab3Clicked()"><a href="#historicTab"
                                                                       data-toggle="tab">Información adicional</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="row" id="infoTab1">
                                        <div class="col-md-12">
                                                <strong>¿Qué es?</strong>
                                            <p>
                                                Measuring the Hive es un proyecto iniciado por un grupo de alumnos (el compadre hack team) de la Escuela Politécnica de Cáceres para el HackForGood 2017 con el fin de
                                                realizar mediciones en tiempo real sobre aglomeraciones y flujos de personas en distintos eventos o zonas de congestión en las ciudades.
                                            </p>
                                        </br>
                                            <strong>¿Cómo funciona?</strong>
                                            <p>
                                                Para conseguir esto capturamos las señales wifi que emiten los smartphones de los ciudadanos, estas señales son completamente anónimas y no llevan asociada
                                                consigo ninguna información personal pero nos permite identificar de forma única a una persona a través de su MAC.
                                            </p>

                                            <p>
                                                Esta captura se realiza utilizando un dispositivo IoT de bajo consumo que puede instalarse de forma muy sencilla en cualquier punto de la ciudad que cuente con
                                                corriente eléctrica.
                                            </p>
                                        </br>
                                                <strong>¿Qué obtenemos?</strong>

                                            <p>
                                                Almacenamos los datos de forma anónima y los procesamos de forma que somos capaces de ubicar, entre otras cosas, la fecha y hora de la captura, distancia a la que
                                                se encontraba del cliente ó el fabricante del dispositivo que lleva consigo.
                                            </p>

                                            <p>
                                                Al tratar con los datos además tenemos dos vistas principales, una en tiempo real, que nos mantiene informados del tránsito de personas al momento
                                                y otra vista basada en el histórico.
                                            </p>

                                            <p>
                                                Esta última es la que se nos antoja más útil puesto que nos permite estudiar las rutinas y afluencia de la gente en distintos puntos de la ciudad o a grandes eventos.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-content">
                                    <div class="row" id="infoTab2">
                                        <div class="col-md-12">

                                            <p><strong>Seguridad ciudadana</strong></p>

                                            <p>
                                                El estudio de las rutinas y el control de la afluencia de la ciudadanía a distintos eventos o lugares, nos permite identificar puntos calientes en las ciudades susceptibles
                                                de requerir una vigilancia particular, en esta plataforma podemos encontrar uno de estos ejemplos. En el festival WOMAD, la policía tuvo acceso a esta información en tiempo
                                                real, lo que les permitió realizar un seguimiento particular este año que nunca antes habían tenido.
                                            </p>

                                            <p><strong>Control de ahorro energético</strong></p>
                                            <p>
                                                No se limita simplemente al entorno de la seguridad; el tipo de informaciónn y la naturaleza en tiempo real de la misma, puede permitirnos, como sociedad, tomar medidas consecuentes
                                                relacionadas al consumo energético, ahorrando en iluminación en ciertas zonas de una ciudad cuando no se detecte gente por los alrededores, cambiando el patrón de los semáforos...
                                            </p>

                                            <p><strong>Pasos futuros</strong></p>

                                            <p>Actualmente nos encontramos deasarrollando la siguiente iteración del proyecto, basada en el estudio de las señas GSM, lo que nos permitirá ampliar el número de paquetes recibidos
                                            y escapar de las limitaciones que la actual implementación basada en señales wifi nos impone.</p>
                                            <p>Por otro lado, y continuando con el desarrollo de la plataforma web, planteamos publicar este panel de desarrollo de modo que una vista de administración permita a futurios usuarios
                                                dar de alta sus propios dispositivos IoT localizándolos donde estén situados y así puedan realizar sus propios estudios basados en esta tecnología.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-content">
                                    <div class="row" id="infoTab3">
                                        <div class="col-md-12">
                                            <p>Con motivo de la celebración del WOMAD y en el marco de nuestra colaboración con la Policía Local de Cáceres realizamos dos informes donde analizamos los datos obtenidos
                                            por la aplicación.
                                            </p>
                                            <p>
                                                En dichos informes se analiza la información recogida, desglosando por horas la afluencia al festival, así como un análisis de la relación establecida entre la meterología y la
                                                asistencia a los conciertos.
                                            </p>
                                            <p>
                                                Estos informes relevan el potencial del proyecto al añadir una capa de Business Intelligence a la hora de analizar los datos, añadiendo gran valor a la información recogida.
                                            </p>
                                            <p> <strong><a href="https://drive.google.com/file/d/0B4_fTb33S5y0SW5renFGZEtneDQ/view?usp=sharing" style="color: #00b0ff" target="_blank">Enlace a informe 11 mayo 10:00AM hasta 13 mayo 05:00AM</a></strong>
                                            </p>
                                            <p> <strong><a href="https://drive.google.com/file/d/0B4_fTb33S5y0Q2JnNXRjRi1OQXc/view?usp=sharing" style="color: #00b0ff" target="_blank">Enlace a informe 11 mayo 10:00AM hasta 14 mayo 05:00AM </a></strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <g:layoutBody/>
            <!-- /page content -->
        </div>

        <div class="sidebar-footer hidden-small" style="background-color: rgba(76, 175, 80, 0.0);">
            <a data-toggle="tooltip" data-placement="top" title="¡Tour!">
                <img style="max-width:100%;
                max-height:100%;" src="https://s3-eu-west-1.amazonaws.com/compadrebucket/explorer.png"
                     onclick="restartTour()">
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Información">
                <img style="max-width:100%;
                max-height:100%;" src="https://s3-eu-west-1.amazonaws.com/compadrebucket/info.png"
                     onclick="displayInformation()">
            </a>
        </div>
    </div>
    %{--<asset:javascript src="jquery-2.2.0.min.js"/>--}%
    %{--<asset:javascript src="datepicker/bootstrap-datepicker.js"/>--}%
    <asset:stylesheet src="datepicker/css/mydatepicker.css"/>
    <asset:stylesheet src="bootstrap/bootstrap-tour-standalone.min.css"/>
    <asset:javascript src="tour/bootstrap-tour-standalone.js"/>
    <!-- jQuery -->
    %{--<asset:javascript src="jquery/dist/jquery.min.js"/>--}%
    <!-- Bootstrap -->
    <asset:javascript src="bootstrap/dist/js/bootstrap.min.js"/>
    <!-- FastClick -->
    <asset:javascript src="fastclick/lib/fastclick.js"/>
    <!-- NProgress -->
    <asset:javascript src="nprogress/nprogress.js"/>
    <!-- Chart.js -->
    <asset:javascript src="Chart.js/dist/Chart.min.js"/>
    <!-- gauge.js -->
    <asset:javascript src="gauge.js/dist/gauge.min.js"/>
    <!-- bootstrap-progressbar -->
    <asset:javascript src="bootstrap-progressbar/bootstrap-progressbar.min.js"/>
    <!-- iCheck -->
    <asset:javascript src="iCheck/icheck.min.js"/>
    <!-- Skycons -->
    <asset:javascript src="skycons/skycons.js"/>
    <!-- Flot -->
    <asset:javascript src="Flot/jquery.flot.js"/>
    <asset:javascript src="Flot/jquery.flot.pie.js"/>
    <asset:javascript src="Flot/jquery.flot.time.js"/>
    <asset:javascript src="Flot/jquery.flot.stack.js"/>
    <asset:javascript src="Flot/jquery.flot.resize.js"/>
    <!-- Flot plugins -->
    <asset:javascript src="flot.orderbars/js/jquery.flot.orderBars.js"/>
    <asset:javascript src="vendors/flot-spline/js/jquery.flot.spline.min.js"/>
    <asset:javascript src="vendors/flot.curvedlines/curvedLines.js"/>
    <!-- DateJS -->
    <asset:javascript src="DateJS/build/date.js"/>
    <!-- JQVMap -->
    <asset:javascript src="jqvmap/dist/jquery.vmap.min.js"/>
    <asset:javascript src="jqvmap/dist/maps/jquery.vmap.world.js"/>
    <asset:javascript src="Djqvmap/examples/js/jquery.vmap.sampledata.js"/>
    <!-- bootstrap-daterangepicker -->
    <asset:javascript src="moment/moment.min.js"/>
    <asset:javascript src="datepicker/daterangepicker.js"/>
    <!-- Custom Theme Scripts -->
    %{--<asset:javascript src="custom.min.js"/>--}%
    <asset:javascript src="dashboard/tour.js"/>
    <asset:javascript src="main/main.js"/>

    <script>
    </script>
</body>
</html>