<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'map.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style>
    /*#myMap {*/
        /*height: 100%;*/
        /*width: 100%;*/
    /*}*/
    </style>
</head>

<body>

<asset:javascript src="jquery-2.2.0.min.js"/>
<asset:stylesheet src="datepicker/css/mydatepicker.css"/>
<asset:javascript src="moment/moment.min.js"/>
<style>
/*#myMap {*/
    /*height: 800px;*/
    /*width: 100%;*/
/*}*/

html, body {
    height: 100%;
    margin: 0;
    padding: 0;
}

/*#map {*/
    /*height: 100%;*/
/*}*/

@keyframes pulsate {
    0% {
        transform: scale(0.1);
        opacity: 0;
    }
    50% {
        opacity: 1;
    }
    100% {
        transform: scale(1.2);
        opacity: 0;
    }
}

.marker {
    width: 40px;
    height: 40px;
    position: absolute;
    top: 130px;
    left: 200px;
    display: block;
}

.pin {
    width: 16px;
    height: 16px;
    position: relative;
    top: 8px;
    left: 8px;
    background: rgba(5, 124, 255, 1);
    border: 4px solid #FFF;
    border-radius: 50%;
    z-index: 1000;
}

.pin-effect {
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    display: block;
    background: rgba(5, 124, 255, 0.6);
    border-radius: 50%;
    opacity: 0;
    animation: pulsate 1s ease-out infinite;
}
</style>

        <div class="row">

            <div class="col-sm-5">

                <ul class="nav nav-pills">
                    <li class="active" onclick="instantTabClicked()"><a href="#instantTab" data-toggle="tab">Ahora mismo</a></li>
                    <li onclick="historicTabClicked()"><a href="#historicTab" data-toggle="tab">Histórico</a>
                </ul>
                </div>

            <div class="col-sm-1">
                <div id="clientSelectorDiv" align="center" style="display: none">
                    <select class="selectpicker" data-width="fit" onchange="clientPickerChanged()"
                            data-live-search="true" id="clientSelector">
                        <g:each in="${clients}">
                            <option id=${it.clientId} value= ${it.clientId}>${it.clientId}</option>
                        </g:each>
                    </select>
                </div>
            </div>
        </div>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-9">
                <div id="myMap" class="twinMaps" style="height: 80vh;max-width:100%;"></div>
            </div>

            <div class="col-md-3">
                %{--<div class="tab-pane fade active in" id="instantTab">--}%
                <div class="row" id="instantPanel" style="display: none">
                    <div class="col-md-12">
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="panel-title">Instantáneo</h4>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseInstant" class="panel-collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div align="center">
                                                <img align="center" style="height: 150px; width: 150px"
                                                     src= ${g.resource(dir: "images", file: "charticon.png", absolute: true)}/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div align="center">
                                                    <button type="button" onclick="instantButtonCliked()"
                                                            class="btn btn-success"
                                                            autofocus="true">Instantáneo</button>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div align="center">
                                                    <button type="button" onclick="historyButtonCliked()"
                                                            class="btn btn-warning">Hace una hora</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 40px">

                                            <h1 id="counter" align="center">78</h1>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                %{--<div class="tab-pane fade" id="historicTab">--}%
                <div class="row" id="historicPanel" style="display: none">

                    <div class="col-md-12">
                        <div class="panel-group">
                            <div class="panel panel-default">

                                <div id="controlPanel" class="panel panel-default">


                                    <div class="panel-heading" style="background-color: #2e6da4; height: 40px" align="center"><p
                                            style="color: white"><strong>Fecha</strong></p></div>

                                    %{--<div class="panel-heading" align="center">Fecha</div>--}%

                                    <div class="row" style="padding-bottom: 10px; padding-top: 10px" id='myDatePickerRow'>
                                        <div class="col-md-12" style="position:relative">
                                            <div class="container" style="position: relative" align="center">
                                                <input id='myDatePicker' value="05/09/2017" onchange="historicDatePickerChanged()"
                                                       data-provide='datepicker' style="background-color: #ffffff"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group">
                            <div class="panel panel-default">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<asset:javascript src="api/APIClient.js"/>

<asset:stylesheet src="bootstrap-select.min.css"/>
<asset:javascript src="bootstrap-select.min.js"/>
<asset:javascript src="bootstrap/js/transition.js"/>
<asset:javascript src="bootstrap/js/collapse.js"/>
<asset:javascript src="js/bootstrap.min.js"/>
<asset:javascript src="bootstrap-datetimepicker.min.js"/>
<asset:javascript src="jquery-ui-1.12.1/jquery-ui.js"/>


<asset:javascript src="map/Socket.js"/>
%{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzyMxm897Kzc1uwMD7bkb0YvTItCswynQ&callback=initMap"></script>--}%
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJhXlMI-KghlBX4afUmWfaNUlQ-wFpZQ4"
        type="text/javascript"></script>
<asset:javascript src="map/richMarker/richmarker.js"/>
<asset:javascript src="map/index.js"/>
<asset:javascript src="map/historic.js"/>
<asset:javascript src="map/map.js"/>


%{--<asset:javascript src="map.js"/>--}%
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
</body>
</html>