package mth.webapp

import com.google.gson.Gson
import grails.converters.JSON
import network.domain.Client
import network.domain.MapPacket

class MapController {

    def clientService

    def index(Integer max) {

        List<Client> clients = clientService.getClients()
//        List<Packet> packets = clientService.getPacketsForDay(clients[0].clientId,"2017-05-09")
        render(view: 'index', model: [clients: clients])
    }

    def mapPacketsByTimestamp(){

        String clientId = params.get(params.keySet()[0])
        String dateFrom = params.get(params.keySet()[1])
        String dateTo = params.get(params.keySet()[2])

        ArrayList<MapPacket> jsonData = clientService.syncGetMapPacketsByTimestamp(clientId, dateTo,dateFrom);

        //Convert Java object to JSON
        String jsonString = new Gson().toJson(jsonData);

        response.setContentType("application/json")
        render jsonString
    }
}

