package mth.webapp

import auth.User
import network.domain.Client
import network.domain.DummyConcert
import network.domain.Packet
import network.domain.PacketHumidity
import network.domain.PacketRain
import network.domain.PacketTemperature
import retrofit2.Call
import retrofit2.Response

import javax.security.auth.callback.Callback

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DashboardController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", loadChart:"POST"]
    grails.gsp.PageRenderer groovyPageRenderer

    def springSecurityService

    def clientService
    def index(Integer max) {

        List<Client> clients = clientService.getClients()
        List<Packet> packets = clientService.getPacketsForDay(clients[0].clientId,"2017-05-09")
        render(view: 'index', model: [clients: clients,
                                      data: packets])
    }

    def loadChart(String param){
        Map params = request.getParameterMap()
        def date = params.get(params.keySet()[1])
        def client = params.get(params.keySet()[0])
        List<Packet> packets = clientService.getPacketsForDay(client[0],date[0])
        render template:"chartTemplate", model: [ data:packets]
    }

    def loadChartWithExtraData(String param){
        Map params = request.getParameterMap()
        Set<String> keys = params.keySet()
        List<PacketTemperature> packetsTemperature = new ArrayList<>()
        List<PacketHumidity> packetsHumidity = new ArrayList<>()
        List<PacketRain> packetsRain = new ArrayList<>()
        List<DummyConcert> packetsConcert = new ArrayList<>()
        def date = params.get(params.keySet()[1])
        def client = params.get(params.keySet()[0])

        if (keys.contains("extra[]")) {
            if (params.get("extra[]")[0].contains("temperature")) {
                packetsTemperature = clientService.getTemperaturPackets(date[0], date[0])
            }
            if (params.get("extra[]")[0].contains("concerts")) {
                packetsConcert = clientService.getDummyConcertPackets(date[0])
            }
            if(params.get("extra[]")[0].contains("humidity")) {
                packetsHumidity = clientService.getHumidityPackets(date[0], date[0])
            }
            if(params.get("extra[]")[0].contains("rain")) {
                packetsRain = clientService.getRainPackets(date[0], date[0])
            }
        }

        List<Packet> packets = clientService.getPacketsForDay(client[0],date[0])
        render template:"chartTemplate", model: [ data:packets,
                                                  dataTemperature: packetsTemperature,
                                                  dataConcerts:packetsConcert,
                                                  dataHumidity: packetsHumidity,
                                                  dataRain: packetsRain]
    }

    def updateChartWithExtraData(){
        Map params = request.getParameterMap()
        Set<String> keys = params.keySet()
        List<PacketTemperature> packetsTemperature = new ArrayList<>()
        List<PacketHumidity> packetsHumidity = new ArrayList<>()
        List<PacketRain> packetsRain = new ArrayList<>()
        List<DummyConcert> packetsConcert = new ArrayList<>()
        def date = params.get(params.keySet()[1])

        if (keys.contains("extra[]")) {
            if (params.get("extra[]")[0].contains("temperature")) {
                packetsTemperature = clientService.getTemperaturPackets(date[0], date[0])
            }
            if (params.get("extra[]")[0].contains("concerts")) {
                packetsConcert = clientService.getDummyConcertPackets(date[0])
            }
            if(params.get("extra[]")[0].contains("humidity")) {
                packetsHumidity = clientService.getHumidityPackets(date[0], date[0])
            }
            if(params.get("extra[]")[0].contains("rain")) {
                packetsRain = clientService.getRainPackets(date[0], date[0])
            }
        }

        render template:"chartTemplate", model: [ dataTemperature: packetsTemperature,
                                                  dataConcerts:packetsConcert,
                                                  dataHumidity: packetsHumidity,
                                                  dataRain: packetsRain]
    }

    def show(User user) {
        respond user
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User user) {
        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view:'create'
            return
        }

        user.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dashboard.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    def edit(User user) {
        respond user
    }

    @Transactional
    def update(User user) {
        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view:'edit'
            return
        }

        user.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'dashboard.label', default: 'User'), user.id])
                redirect user
            }
            '*'{ respond user, [status: OK] }
        }
    }

    @Transactional
    def delete(User user) {

        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        user.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'dashboard.label', default: 'User'), user.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dashboard.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
