class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(redirect: "/dashboard")
        "/font/roboto/Roboto-Regular.ttf"(redirect: "/dashboard")
        "500"(view:'/error')
        "404"(view:'/notFound')

    }
}
