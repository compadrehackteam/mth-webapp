package mth.webapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.transaction.jta.*
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration

@EnableAutoConfiguration(exclude = [JtaAutoConfiguration])
@SpringBootApplication
class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }
}