package mth.webapp

import grails.transaction.Transactional
import network.apiclient.ApiClient
import network.apiclient.ApiService
import network.domain.Client
import network.domain.DummyConcert
import network.domain.Packet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Transactional
class ClientService {

    ApiService apiService

    def getAsyncClients(final Callback<List<Client>> callback) {
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        apiClient.getClients(new Callback<List<Client>>() {
            @Override
            void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                callback.onResponse(call, response)
            }

            @Override
            void onFailure(Call<List<Client>> call, Throwable throwable) {
                callback.onFailure(call, throwable)
            }
        })
    }

    def getAsyncPacketsForDay(String idClient, String date, final Callback<List<Packet>> callback){
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        apiClient.getPacketsByDate(idClient, date, new Callback<List<Packet>>() {
            @Override
            void onResponse(Call<List<Packet>> call, Response<List<Packet>> response) {
                callback.onResponse(call, response)
            }

            @Override
            void onFailure(Call<List<Packet>> call, Throwable throwable) {
                callback.onFailure(call, throwable)
            }
        })
    }

    def getClients(){
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        return apiClient.syncGetClient()
    }

    def getPacketsForDay(String idClient, String date){
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        return apiClient.syncGetPacketsForDay(idClient, date)
    }

    def getTemperaturPackets(String dateFrom, String dateTo) {
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        return apiClient.syncGetPacketTemperature(dateFrom, dateTo)
    }

    def getHumidityPackets(String dateFrom, String dateTo) {
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        return apiClient.syncGetPacketHumidity(dateFrom, dateTo)
    }

    def getRainPackets (String dateFrom, String dateTo) {
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        return apiClient.syncGetPacketRain(dateFrom, dateTo)
    }

    def syncGetMapPacketsByTimestamp (String clientId, String dateFrom, String dateTo){
        apiService = new ApiService()
        ApiClient apiClient = new ApiClient(apiService)
        return apiClient.syncGetMapPacketsByTimestamp(clientId, dateFrom, dateTo)
    }

    def getDummyConcertPackets(String date){
        List<DummyConcert> concerts = new ArrayList<>()
        concerts.add(new DummyConcert("00:00", 0))
        concerts.add(new DummyConcert("01:00", 0))
        concerts.add(new DummyConcert("02:00", 0))
        concerts.add(new DummyConcert("03:00", 0))
        concerts.add(new DummyConcert("04:00", 0))
        concerts.add(new DummyConcert("05:00", 0))
        concerts.add(new DummyConcert("06:00", 0))
        concerts.add(new DummyConcert("07:00", 0))
        concerts.add(new DummyConcert("08:00", 0))
        concerts.add(new DummyConcert("09:00", 0))
        concerts.add(new DummyConcert("10:00", 0))
        concerts.add(new DummyConcert("11:00", 0))
        concerts.add(new DummyConcert("12:00", 0))
        concerts.add(new DummyConcert("13:00", 0))
        concerts.add(new DummyConcert("14:00", 0))
        concerts.add(new DummyConcert("15:00", 0))
        concerts.add(new DummyConcert("16:00", 0))
        concerts.add(new DummyConcert("17:00", 0))
        concerts.add(new DummyConcert("18:00", 0))
        concerts.add(new DummyConcert("19:00", 0))
        concerts.add(new DummyConcert("20:00", 0))
        concerts.add(new DummyConcert("21:00", 0))
        concerts.add(new DummyConcert("22:00", 0))
        concerts.add(new DummyConcert("23:00", 0))
        if(date.equals("2017-05-11")) {
            concerts = new ArrayList<>()
            concerts.add(new DummyConcert("00:00", 0))
            concerts.add(new DummyConcert("01:00", 0))
            concerts.add(new DummyConcert("02:00", 0))
            concerts.add(new DummyConcert("03:00", 0))
            concerts.add(new DummyConcert("04:00", 0))
            concerts.add(new DummyConcert("05:00", 0))
            concerts.add(new DummyConcert("06:00", 0))
            concerts.add(new DummyConcert("07:00", 0))
            concerts.add(new DummyConcert("08:00", 0))
            concerts.add(new DummyConcert("09:00", 0))
            concerts.add(new DummyConcert("10:00", 0))
            concerts.add(new DummyConcert("11:00", 0))
            concerts.add(new DummyConcert("12:00", 0))
            concerts.add(new DummyConcert("13:00", 0))
            concerts.add(new DummyConcert("14:00", 0))
            concerts.add(new DummyConcert("15:00", 0))
            concerts.add(new DummyConcert("16:00", 0))
            concerts.add(new DummyConcert("17:00", 0))
            concerts.add(new DummyConcert("18:00", 0))
            concerts.add(new DummyConcert("19:00", 0))
            concerts.add(new DummyConcert("20:00", 1))
            concerts.add(new DummyConcert("21:00", 1))
            concerts.add(new DummyConcert("22:00", 1))
            concerts.add(new DummyConcert("23:00", 1))
        }
        if(date.equals("2017-05-12")) {
            concerts = new ArrayList<>()
            concerts.add(new DummyConcert("00:00", 0))
            concerts.add(new DummyConcert("01:00", 0))
            concerts.add(new DummyConcert("02:00", 0))
            concerts.add(new DummyConcert("03:00", 0))
            concerts.add(new DummyConcert("04:00", 0))
            concerts.add(new DummyConcert("05:00", 0))
            concerts.add(new DummyConcert("06:00", 0))
            concerts.add(new DummyConcert("07:00", 0))
            concerts.add(new DummyConcert("08:00", 0))
            concerts.add(new DummyConcert("09:00", 0))
            concerts.add(new DummyConcert("10:00", 0))
            concerts.add(new DummyConcert("11:00", 0))
            concerts.add(new DummyConcert("12:00", 0))
            concerts.add(new DummyConcert("13:00", 0))
            concerts.add(new DummyConcert("14:00", 0))
            concerts.add(new DummyConcert("15:00", 0))
            concerts.add(new DummyConcert("16:00", 0))
            concerts.add(new DummyConcert("17:00", 0))
            concerts.add(new DummyConcert("18:00", 0))
            concerts.add(new DummyConcert("19:00", 1))
            concerts.add(new DummyConcert("20:00", 1))
            concerts.add(new DummyConcert("21:00", 1))
            concerts.add(new DummyConcert("22:00", 1))
            concerts.add(new DummyConcert("23:00", 1))
        }
        if(date.equals("2017-05-13")) {
            concerts = new ArrayList<>()
            concerts.add(new DummyConcert("00:00", 1))
            concerts.add(new DummyConcert("01:00", 1))
            concerts.add(new DummyConcert("02:00", 1))
            concerts.add(new DummyConcert("03:00", 0))
            concerts.add(new DummyConcert("04:00", 0))
            concerts.add(new DummyConcert("05:00", 0))
            concerts.add(new DummyConcert("06:00", 0))
            concerts.add(new DummyConcert("07:00", 0))
            concerts.add(new DummyConcert("08:00", 0))
            concerts.add(new DummyConcert("09:00", 0))
            concerts.add(new DummyConcert("10:00", 0))
            concerts.add(new DummyConcert("11:00", 0))
            concerts.add(new DummyConcert("12:00", 0))
            concerts.add(new DummyConcert("13:00", 0))
            concerts.add(new DummyConcert("14:00", 0))
            concerts.add(new DummyConcert("15:00", 0))
            concerts.add(new DummyConcert("16:00", 0))
            concerts.add(new DummyConcert("17:00", 0))
            concerts.add(new DummyConcert("18:00", 0))
            concerts.add(new DummyConcert("19:00", 1))
            concerts.add(new DummyConcert("20:00", 1))
            concerts.add(new DummyConcert("21:00", 1))
            concerts.add(new DummyConcert("22:00", 1))
            concerts.add(new DummyConcert("23:00", 1))
        }
        if(date.equals("2017-05-14")) {
            concerts = new ArrayList<>()
            concerts.add(new DummyConcert("00:00", 1))
            concerts.add(new DummyConcert("01:00", 1))
            concerts.add(new DummyConcert("02:00", 1))
            concerts.add(new DummyConcert("03:00", 0))
            concerts.add(new DummyConcert("04:00", 0))
            concerts.add(new DummyConcert("05:00", 0))
            concerts.add(new DummyConcert("06:00", 0))
            concerts.add(new DummyConcert("07:00", 0))
            concerts.add(new DummyConcert("08:00", 0))
            concerts.add(new DummyConcert("09:00", 0))
            concerts.add(new DummyConcert("10:00", 0))
            concerts.add(new DummyConcert("11:00", 0))
            concerts.add(new DummyConcert("12:00", 0))
            concerts.add(new DummyConcert("13:00", 0))
            concerts.add(new DummyConcert("14:00", 0))
            concerts.add(new DummyConcert("15:00", 0))
            concerts.add(new DummyConcert("16:00", 0))
            concerts.add(new DummyConcert("17:00", 0))
            concerts.add(new DummyConcert("18:00", 0))
            concerts.add(new DummyConcert("19:00", 0))
            concerts.add(new DummyConcert("20:00", 0))
            concerts.add(new DummyConcert("21:00", 0))
            concerts.add(new DummyConcert("22:00", 0))
            concerts.add(new DummyConcert("23:00", 0))
        }
        return concerts
    }
}
