/**
 * Created by pedro on 4/09/17.
 */
$( document ).ready(function() {
    infoTab1Clicked()
});

function infoTab1Clicked(){
    $('#infoTab1').css('display', 'inline');
    $('#infoTab2').css('display', 'none');
    $('#infoTab3').css('display', 'none');
}

function infoTab2Clicked(){
    $('#infoTab2').css('display', 'inline');
    $('#infoTab1').css('display', 'none');
    $('#infoTab3').css('display', 'none');

}

function infoTab3Clicked(){
    $('#infoTab3').css('display', 'inline');
    $('#infoTab1').css('display', 'none');
    $('#infoTab2').css('display', 'none');
}