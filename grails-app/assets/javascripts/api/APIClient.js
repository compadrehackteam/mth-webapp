/**
 * Created by pedro on 10/03/17.
 */
var baseURL = "http://ec2-18-220-192-200.us-east-2.compute.amazonaws.com:8585";
var clientsURL = "/api/getClients/";
var clientDataURL = "/api/getClientData/";
var packetsByDateURL = "/getPacketsByDate/";
var lastPacketReceived = "/api/getNumberOfPackets";
var maxByDateURL = "/api/getMaxByDate";
var clientParametrizedURL = "/api/getClientDataParametrized";


function fetchClients(callback) {

    var clientList = {};

    $.ajax({
        type: 'GET',
        url: baseURL+clientsURL,
        data: (clientList),
        dataType: 'json',
        success: function (clientList) {
            callback(clientList, null);
        }
    });
}

/**
 * Generic Ajax caller.
 * @param url
 * @returns {*}
 */
function ajaxCaller(url){
    var data = {};
    data["query"] = $("#query").val();
    return $.ajax({
        type : "GET",
        contentType : "application/json",
        url : url,
        data : (data),
        dataType : 'json',
        timeout : 100000
    });
}




