/**
 * Created by pedro on 26/08/17.
 */

var customIcon = "./resources/lightHouse2.png";
var markers = {};
var mapMarkers = [];
var markerId = 0;

var isInstant = true;
var selectedClient;
var globalMap;
var marker;
var index = 0;

function mapDatePickerChanged() {
}

$( document ).ready(function() {
    $('#historicPanel').css('display', 'none');
    $('#instantPanel').css('display', 'none');
    $('#clientSelectorDiv').css('display', 'none');
    $("#instantCheckBox").attr("disabled", true);
    createInstantMap();

});

function centerInstantMap () {
    globalMap.setCenter(getHotelLoc());
}

function instantTabClicked(){
    isInstant = true;
    centerInstantMap();
    deleteAllMarkers();
    $('#instantPanel').css('display', 'none');
    $('#historicPanel').css('display', 'none');
}

function createInstantMap () {
    var icon = {
        url: customIcon,
        anchor: new google.maps.Point(25,50),
        scaledSize: new google.maps.Size(50,50)
    };

    // globalMap = new google.maps.Map(document.getElementById('myMap'), {
    globalMap = new google.maps.Map(document.getElementsByClassName('twinMaps')[0], {
        zoom: 20,
        center: getHotelLoc()
    });

    // var marker = new google.maps.Marker({
    //     position: getHotelLoc(),
    //     //icon: icon,
    //     map: globalMap
    // });
}

function setPos() {
    // console.log("index " + index);
    if (index < positions_length){
        var latlng = new google.maps.LatLng(positions[index].lat, positions[index].lng);
        marker.setPosition(latlng);
        // map.setCenter(latlng);
        index ++;
    } else {
        clearInterval(timer);
    }
}

function instantCheckBoxClicked(){
    if($('#instantCheckBox').is(':checked')){
        $('#collapseInstant').collapse({
            toggle: true
        });
        $('#collapseHistoric').collapse({
            toggle: false
        });
        $("#instantCheckBox").attr("disabled", true);
        $("#historyCheckBox").removeAttr("disabled");
    }
}

function instantButtonCliked() {
    isInstant = true;

}

function historyCheckBoxClicked () {
    if($('#historyCheckBox').is(':checked')){
        $('#collapseHistoric').collapse({
            toggle: true
        });
        $('#collapseInstant').collapse({
            toggle: false
        });
        $("#historyCheckBox").attr("disabled", true);
        $("#instantCheckBox").removeAttr("disabled");
    }}

function historyButtonCliked() {
    isInstant = false;

}

// function updateValue(item){
//     if(item.distance < 40){
//         if(getMarkersMap()[item.mac] === undefined){
//             console.log("mac undefined")
//             var destinationPoint = calculatePosition(item.distance);
//             updateMapWithNewData(item, destinationPoint);
//         }else{
//             console.log("mac defined")
//             getMarkersMap()[item.mac] = item.distance;
//             getMarkersArray().forEach(function (storedMarker) {
//                 if(storedMarker.id === item.mac){
//                     // storedMarker.setMap(null);
//                     getMarkersMap()[item.mac] = null;
//                 }
//             });
//             updateMap(item);
//         }
//     }
//     else{
//         if(storedMarker.id == item.mac){
//             storedMarker.setMap(null);
//             getMarkersMap()[item.mac] = null;
//         }
//     }
// }

function updateSelectedClient(){
    var selector = document.getElementById("clientPicker");
    selectedClient = selector.value;
}

/**
 * Created by pedro on 10/03/17.
 */


function updateMapWithNewData(data , destinationPoint){
    var markerPosition = new google.maps.LatLng(destinationPoint.lat, destinationPoint.lng);
    // addmarker(markerPosition);

    marker = new RichMarker({
        map: globalMap,
        position: markerPosition,
        draggable: false,
        flat: true,
        anchor: RichMarkerPosition.MIDDLE,
        content: '<div class="pin"></div><div class="pin-effect"></div>'
    });
    marker.setPosition(markerPosition);
    // richMarker.setPosition(markerPosition);


    //timer = setInterval("setPos()",1000);


    //marker.id = markerId;
    //  markerId++;
    markers[data.mac] = data.distance;
    marker.id = data.mac;
    mapMarkers.push(marker);

}

var latlng = new google.maps.LatLng(42.745334, 12.738430);

function addmarker(latilongi) {
    var marker = new google.maps.Marker({
        position: latilongi,
        title: 'new marker',
        draggable: true,
        map: globalMap
    });
    // map.setCenter(marker.getPosition())
}

$('#btnaddmarker').on('click', function() {
    addmarker(latlng)
});

function deleteMarker(id) {

    //Find and remove the marker from the Array
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
            //Remove the marker from Map
            markers[i].setMap(null);

            //Remove the marker from array.
            markers.splice(i, 1);
            return;
        }
    }
}

function getMarkersMap(){
    return markers;
}


function getMarkersArray(){
    return mapMarkers;
}

function setMarkersArray(auxMarkersArray){
    mapMarkers = auxMarkersArray;
}

function setPos() {
    // console.log("index " + index);
    if (index < positions_length){
        var latlng = new google.maps.LatLng(positions[index].lat, positions[index].lng);
        marker.setPosition(latlng);
        // map.setCenter(latlng);
        index ++;
    } else {
        clearInterval(timer);
    }
}

var RichMarkerPosition = {
    'TOP_LEFT': 1,
    'TOP': 2,
    'TOP_RIGHT': 3,
    'LEFT': 4,
    'MIDDLE': 5,
    'RIGHT': 6,
    'BOTTOM_LEFT': 7,
    'BOTTOM': 8,
    'BOTTOM_RIGHT': 9
};
