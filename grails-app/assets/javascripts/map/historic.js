/**
 * Created by pedro on 3/09/17.
 */

var historicMap;

function historicTabClicked() {
    // console.log("historic tab clicked");
    isInstant = false;
    createHistoricMap()
    $('#myDatePicker').datepicker();
    $('#clientSelectorDiv').css('display', 'inline');
    $('#historicPanel').css('display', 'inline');
    $('#instantPanel').css('display', 'none');
    feedPicker();
    deleteAllMarkers();
    //  createHistoricMap();±
    historicClientPickerChanged()
}

function parseDate(dateAsArray) {
    return dateAsArray[2] + "-" + dateAsArray[0] + "-" + dateAsArray[1];
}

function historicDatePickerChanged(){

    var date = parseDate($('#myDatePicker').datepicker().val().split("/"));

    var client = $('#clientSelector').selectpicker().val();

    // Call the API to retrieve the data for MAP

    $.ajax({
        type: "GET",
        url: "/map/mapPacketsByTimestamp",
        data: { param1: client ,param2: date+"T10:00", param3: date+"T11:00"},
        success: function (response) {
            clearMapAndDrawHistoricElements(response)
        }
    });
}

function deleteAllMarkers(){

    for(var i =0; i<mapMarkers.length;i++){
        mapMarkers[i].setMap(null);
    }
    mapMarkers = []
    markers = {};
}

function clearMapAndDrawHistoricElements(jsonList){

    jsonList.forEach(function (item) {
        updateMap(item);
    });

}
function createHistoricMap () {
    globalMap.setCenter(getEpccLoc());
    // var icon = {
    //     url: customIcon,
    //     anchor: new google.maps.Point(25,50),
    //     scaledSize: new google.maps.Size(50,50)
    // };
    //
    //
    // globalMap = new google.maps.Map(document.getElementsByClassName('twinMaps')[1], {
    //     zoom: 20,
    //     center: getEpccLoc()
    // });
    // var marker = new google.maps.Marker({
    //     position: getEpccLoc(),
    //     //icon: icon,
    //     map: historicMap
    // });
}

function feedPicker(){
    fetchClients(function (clientList, err) {

        selectedClient = clientList[1].clientId;
        var selector = document.getElementById("clientPicker");

        for(var i = 0; i<clientList.length; i++){
            var opt = document.createElement('option');
            opt.value = clientList[i].clientId;
            opt.innerHTML = clientList[i].clientId;
            selector.appendChild(opt);
        }
        $("#clientPicker").load(location.href + " #clientPicker");
    });
}

function historicClientPickerChanged (){

}