/**
 * Created by pedro on 10/05/17.
 */
/**
 *
 * @param date
 * @param minutes
 */
function dateMinusMinutes(date, minuteToSubstract, withHour){
    if(minuteToSubstract > 0){
        var minutesAsMiliseconds = minuteToSubstract * 60000;
        //(120*60000) apaño para el serviodr
        date = new Date(date - minutesAsMiliseconds - (120*60000));
    }

    var dateAsString = date.getFullYear();
    dateAsString += '-';

    var month = date.getMonth();
    month ++;
    if(month < 10){
        dateAsString += '0';
    }
    dateAsString  += month;
    dateAsString += '-';

    var day = date.getUTCDate();
    if(day <10){
        dateAsString += '0';
    }
    dateAsString  += day;
    if(withHour){
        dateAsString += 'T';

        var hours = date.getHours();
        if(hours < 10){
            dateAsString += '0';
        }
        dateAsString  += hours;
        dateAsString += ':';

        var minutes = date.getMinutes();
        if(minutes < 10){
            dateAsString += '0';
        }
        dateAsString  += minutes;
        dateAsString += ':';

        var seconds = date.getSeconds();
        if(seconds < 10){
            dateAsString += 0;
        }
        dateAsString += seconds;
    }
    return dateAsString;
}

function updateChart (filter, date, idClient) {
    $('#circle').show();
    $( ".chartTemplate" ).load( "/dashboard/updateChartWithExtraData", {"client": [idClient], "date" : [date], "extra": [filter]}, function() {
        $('#circle').hide();
    });
}

function loadChart(idClient, date){
    $('#circle').show();
    removeAllDataset()
    var hasExtraData = false;
    var extraData ="";
    if($('#temperatureCheckbox').is(':checked')) {
        hasExtraData = true;
        extraData += "temperature"
    }

    if($('#concertsCheckbox').is(':checked')) {
        hasExtraData = true;
        extraData += "concerts"
    }

    if($('#humidityCheckbox').is(':checked')) {
        hasExtraData = true;
        extraData += "humidity"
    }
    if($('#rainCheckbox').is(':checked')) {
        hasExtraData = true;
        extraData += "rain"
    }


    if(hasExtraData) {
        $( ".chartTemplate" ).load( "/dashboard/loadChartWithExtraData", {"client": [idClient], "date" : [date], "extra": [extraData]}, function() {
            $('#circle').hide();
        });
    } else {
        $( ".chartTemplate" ).load( "/dashboard/loadChart", {"client": [idClient], "date":[date]}, function() {
            $('#circle').hide();
        });
    }
}