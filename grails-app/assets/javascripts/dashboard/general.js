/**
 * Created by ricardo on 10/03/17.
 */
var barChart;
var clientName = "";
var loadChartUrl = "dashboard/loadChart";

var availableDates = ["9-5-2011", "14-5-2011", "15-5-2011"];

function createProgressCircle() {

    var opts = {
        lines: 17 // The number of lines to draw
        , length: 0 // The length of each line
        , width: 8 // The line thickness
        , radius: 12 // The radius of the inner circle
        , scale: 0.40 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#000' // #rgb or #rrggbb or array of colors
        , opacity: 0 // Opacity of the lines
        , rotate: 55 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1.7 // Rounds per second
        , trail: 71 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '51%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
    }
    var target = document.getElementById('circle')
    var spinner = new Spinner(opts).spin(target);
}

function available(date) {
    dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
    if ($.inArray(dmy, availableDates) != -1) {
        return [true, "", "Available"];
    } else {
        return [false, "", "unAvailable"];
    }
}

$(document).ready(function () {
    $('#myDatePicker').datepicker();
    $('#circle').hide();
    createProgressCircle();
});


function updateSelectedClient() {
    var selector = document.getElementById("clientSelector");
    selectedClient = selector.value;
    datePickerChanged();
}

function clientPickerChanged() {
    datePickerChanged();
}


function datePickerChanged() {
    var dateTypeVar = $('#myDatePicker').datepicker().val();
    var dateAsArray = dateTypeVar.split("/");
    var client = $('#clientSelector').val();
    var selectedDate = parseDate(dateAsArray);
    loadChart(client, selectedDate);
}



function checkboxClicked(filter){
    var checkBoxId = "#" + filter + "Checkbox";
    if($(checkBoxId).is(':checked')) {
        var dateTypeVar = $('#myDatePicker').datepicker().val();
        var dateAsArray = dateTypeVar.split("/");
        var client = $('#clientSelector').val();
        var selectedDate = parseDate(dateAsArray);
        updateChart(filter, selectedDate, client)
    } else {
        removeDataset(filter);
    }
}

function parseDate(dateAsArray) {
    return dateAsArray[2] + "-" + dateAsArray[0] + "-" + dateAsArray[1];
}

function inflateBarCharView(people, temperatures, concerts, humidity, rain) {
    var dataindex = [];
    var dataPeople = [];
    var dataTemperature = [];
    var dataHumidity = [];
    var dataRain = [];
    var dataConcerts = [];

    people.forEach(function (object) {
        dataindex.push(object.fhour);
        dataPeople.push((object.clients).toFixed(2));
    });



    var lineData = {
        labels: dataindex,
        datasets: []
    };

    if (barChart !== undefined) {
        return
        //barChart.destroy();
    }

    var ctx = document.getElementById("lineChart").getContext("2d");
    barChart = new Chart(ctx, {
        type: "line",
        data: lineData,
        options: {
            xAxes: [{gridLines: {display: false}}],
            display: false,
            scales: {
                    yAxes: [
                        {
                            id: 'A',
                            type: 'linear',
                            position: 'left',
                            ticks: {
                                min: 0
                            },
                        gridLines: {
                            display: false
                        }
                    },
                    {
                        id: 'B',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            max: 40,
                            min: 0,
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }, {
                        id: 'C',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            max: 1,
                            min: 0,
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }, {
                        id: 'D',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            max: 100,
                            min: 0,
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }, {
                        id: 'E',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            max: 5,
                            min: 0,
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }
                    ]
            }
        }
    });

    barChart.update();
}
function applyChartColors(dataSet, backgroundColor, borderColor) {
    barChart.config.data.datasets[dataSet].backgroundColor = backgroundColor;
    barChart.config.data.datasets[dataSet].borderColor = borderColor;
}




