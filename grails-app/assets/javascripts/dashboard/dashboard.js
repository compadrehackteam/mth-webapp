$(document).ready(function() {
    $('#datetimepicker1').datetimepicker();
    startTour();
});

/**
 * Converts the actual date to string with format "YYYY-MM-DD"
 *
 * @returns
 */
function actualDateToString() {

    var nowTemp = new Date();
    var stringDate = "";

    if ((nowTemp.getMonth() + 1) < 10) {
        stringDate = nowTemp.getFullYear() + "-" + "0"
            + (nowTemp.getMonth() + 1);
    } else {
        stringDate = nowTemp.getFullYear() + "-" + (nowTemp.getMonth() + 1);
    }

    if (nowTemp.getDate() < 10) {
        stringDate = stringDate + "-" + "0" + nowTemp.getDate();
    } else {
        stringDate = stringDate + "-" + nowTemp.getDate();
    }
    return (stringDate.valueOf());

}
