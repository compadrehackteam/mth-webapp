var isHome = true;
var tour = new Tour({
    orphan: true,
    smartPlacement:true,
    container: 'body',
    template: "<div class='popover tour'>"
    +" <div class='arrow'></div>"
    +" <h3 class='popover-title' style='background-color: #2e6da4; color: white'></h3>"
    +   " <div class='popover-content'></div>"
    +   " <div class='popover-navigation'>"
    +       " <button class='btn btn-primary' data-role='prev'><img height='15' width='15' src=\"https://s3-eu-west-1.amazonaws.com/compadrebucket/left.png\"'/></button>"
    +       " <button class='btn btn-primary' data-role='next'><img height='15' width='15' src=\"https://s3-eu-west-1.amazonaws.com/compadrebucket/right.png\"'/></button>"
    +       " <button class='btn btn-danger' data-role='end'><img height='15' width='15' src=\"https://s3-eu-west-1.amazonaws.com/compadrebucket/times.png\"'/></i></button>"
    +" </div>",
    steps: [
        {
            element: "#logoCompadre",
            orphan: true,
            placement: "right",
            smartPlacement: true,
            title: "¡Bienvenidos a Measuring the Hive!",
            content: "¡Hola! Somos el Compadre Hack Team " + "<a href=" + "https://twitter.com/compadrehack" +">@compadrehack</a>" +
            "Vamos a hacer un pequeño tour por nuestra aplicación, sed bienvenidos." + "<br/>" +
            "Primero explicaremos en qué consiste esta plataforma.",
            onNext:function() {
                if (!window.location.href.includes('dashboard')) {
                    document.location.href = "/dashboard";
                }
            }
        },
        {
            element: "#chartSection",
            title: "¿Measuring the Hive?",
            content: "Se trata de un sistema que aprovecha las señales wifi que emiten los smartphones para poder realizar métricas " +
            "sobre la gente que pasa por un lugar en tiempo real, además, almacenamos esta información para realizar estudios a posteriori." + "<br/>" +
            "Todo esto está muy bien, pero... ¿funciona?",
            onNext: function (tour) {$("#modalImage").attr('src', "https://s3-eu-west-1.amazonaws.com/compadrebucket/womad1.png");
                $('#modalTour').modal('show')}
        },
        {
            element: "#modalHeader",
            title: "¡Vaya!",
            content: "En un primer momento recogimos datos durante la celebración del HackForGood en nuestra sede de Cáceres, disponibles en el cliente" +
            "epcc en las fechas correspondientes (10 y 11 de Marzo), donde se puede apreciar las horas puntas de máxima afluencia (comida y anuncio de los ganadores, " +
            "además de los intervalos en los la gente empezaba a acudir a la sede." + "<br/>" +
            "Una vez superada esta prueba y gracias a la colaboración de la Policía Municipal y el Ayuntamiento de Cáceres tuvimos la oportunidad de probar el sistema " +
            "en un entorno real, el festival WOMAD que se celebra, entre otros sitios, en esta ciudad",
            onNext: function (tour) {$("#modalImage").attr('src', 'https://s3-eu-west-1.amazonaws.com/compadrebucket/womad2.png')},
            onPrev: function (tour) {$('#modalTour').modal('hide')}
        },
        {
            element: "#modalBody",
            title: "Esto está muy bien, pero ¿funcionó?",
            placement: "left",
            content: "¡Y tanto! Pudimos medir la asistencia de un modo bastante fiable, tanto que la policía y la alcaldesa, " +
            "dieron nuestros datos a la prensa al finalizar el evento. 150.000 asistentes pasando por la plaza de Cáceres, ¿te imaginas?",
            onPrev: function (tour) {$("#modalImage").attr('src', 'https://s3-eu-west-1.amazonaws.com/compadrebucket/womad1.png')},
            onNext: function (tour) {$('#modalTour').modal('hide');}
        },
        {
            element: "#menuHome",
            backdrop: false,
            title: "Así que... Measuring the Hive",
            placement: "auto",
            content: "¡Sí! Vamos a ello. Esta es nuestra plataforma, aquí podemos ver los datos que hemos recogido.",
            onPrev: function (tour) {$('#modalTour').modal('show')}
        },
        {
            element: "#controlPanel",
            backdrop: true,
            title: "Mesauring the Hive",
            placement: "left",
            content: "Aquí está el panel de control, donde puedes seleccionar el dispositivo desde el que realizamos las medidas. " +
            "En ese momento teníamos dos clientes (los dispositivos de captura) funcionando, puedes escoger cualquiera, también puedes seleccionar la fecha que quieras " +
            "y visualizar así los datos inferidos de la información almacenada."
        },
        {
            element: "#clientSelectorDiv",
            backdrop: true,
            title: "Mesauring the Hive",
            placement: "auto",
            content: "Esta fue nuestra primera prueba de gran magnitud y lamentablemente el segundo dispositivo (plaza2) tuvo que superar... algunas dificultades " +
            "es por eso que sus datos puedan parecer algo extraños."
        },
        {
            element: "#myDatePickerRow",
            backdrop: true,
            title: "Mesauring the Hive",
            placement: "left",
            content: "¡Cuando selecciones la fecha ten en cuenta el momento en el que se celebró el festival! Aunque no lo sepas seguro " +
            "que en cuanto juegues un poco te das cuenta de cuándo se reunió más gente. Por nuestra parte estuvimos tomando datos desde " +
            "el 9 hasta el 14 de Mayo, así que siéntete libre."
        },
        {
            element: "#metricsMeasures",
            backdrop: true,
            title: "Mesauring the Hive",
            placement: "left",
            content: "Para terminar con esta sección, hemos preparado algunos ejemplos de cómo puede inferir cierta información con " +
            "la afluencia de personal al evento, en este caso datos metorológicos y las horas en las que hubo conciertos, ¡verás alguna relación curiosa!"
        },
        {
            element: "#menuMap",
            backdrop: false,
            title: "Mesauring the Hive",
            placement: "right",
            content: "Por último, aquí tienes una pequeña prueba de como funciona el sistema en tiempo real, seguramente no veas tanta gente " +
            "como en el festival, pero te puedes hacer una idea. ¡A jugar!"
        }
    ]});

function displayInformation() {
    $('#modalInfo').modal('show')
}
function closeInfo() {
}
function startTour() {
    tour.init();
    // tour.start();
    tour.restart();
}
function restartTour(){
    tour.restart();
}
function menuHomeClicked(){
    if(!isHome) {
        isHome = true;
        // $('#menuHome').css('background','#2e6da4');
        // $('#menuMap').css('background','#2A3F54');
    }
}
function menuMapClicked(){
    if(isHome) {
        isHome = false;
        // $('#menuMap').css('background','#2e6da4');
        // $('#menuHome').backgroundColor('#2A3F54');
    }
}
$('li').mouseover(function()
{
    if ($('li ul li:hover').length)
    {
        $('li ul li:hover').css('background','#2e6da4');
    }
    else
    {
        $('li:hover').css('background','#2e6da4');
    }
});
$('li').mouseout(function()
{
    $(this).css('background', 'transparent');
});