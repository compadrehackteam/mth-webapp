var globalChart;

function reloadChart (chart) {
    var dataindex = [];
    globalChart = chart
    // var chart = getBarChartObject();
    if(people.length > 0){
        var dataPeople = [];
        people.forEach(function (object) {
            dataindex.push(object.fhour);
            dataPeople.push((object.clients).toFixed(2));
        });
        chart.data.datasets.push({
            ids: 'people',
            label: 'Asistentes',
            data: dataPeople,
            yAxisID: 'A',
            backgroundColor:  "rgba(124, 179, 66, 0.2)",
            borderColor:  "rgba(124, 179, 66, 1)"
        });
    }if(temperatures.length > 0){
        var dataTemp = [];
        temperatures.forEach(function (object) {
            dataTemp.push((object.temp).toFixed(2));
        });
        chart.data.datasets.push({
            ids: 'temperature',
            label: 'Temperatura',
            data: dataTemp,
            yAxisID: 'B',
            backgroundColor:  "rgba(255, 99, 132, 0.2)",
            borderColor:  "rgba(255, 99, 132, 1)"
        });
    }if(concerts.length > 0){
        var dataConcert = [];
        concerts.forEach(function (object) {
            dataConcert.push((object.concert).toFixed(2));
        });
        chart.data.datasets.push({
            ids: 'concerts',
            label: 'Conciertos',
            data: dataConcert,
            backgroundColor:  "rgba(251, 192, 45, 0.2)",
            borderColor: "rgba(251, 192, 45, 1)",
            yAxisID: 'C'
        });
    }if(rain.length > 0){
        var dataRain = [];
        rain.forEach(function (object) {
            dataRain.push((object.rain).toFixed(2));
        });
        chart.data.datasets.push({
            ids: 'rain',
            label: 'Lluvia',
            data: dataRain,
            yAxisID: 'E',
            backgroundColor:  "rgba(0,150,136 ,0.0)",
            borderColor: "rgba(0,150,136 ,1)"
        });
    }
    if(humidity.length > 0){
        var dataHumidity = [];
        humidity.forEach(function (object) {
            dataHumidity.push((object.humidity).toFixed(2));
        });
        chart.data.datasets.push({
            ids: 'humidity',
            label: 'Humedad',
            data: dataHumidity,
            yAxisID: 'D',
            backgroundColor:  "rgba(3, 169, 244,  0.2)",
            borderColor: "rgba(3, 169, 244,  1)"
        });
    }
    chart.update();

    // inflateBarCharView(people, temperatures, concerts, humidity, rain);
};

function removeDataset(filter) {
    var removalIndex = -1; //Locate index of ds1
    for(var i = 0; i< globalChart.data.datasets.length; i++){
        if(globalChart.data.datasets[i].ids == filter) {
            removalIndex = i;
        }
    }
    if(removalIndex >= 0) { //make sure this element exists in the array
        globalChart.data.datasets.splice(removalIndex, 1);
    }
    globalChart.update();
}

function removeAllDataset() {
    for(var i = 0; i<= globalChart.data.datasets.length; i++){
        globalChart.data.datasets.pop();
    }
    globalChart.data.datasets.pop();
    globalChart.update();
}


