package network.apiclient;

import network.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by pedro on 27/08/17.
 */
public class ApiClient {

    final ApiService apiService;


    @Autowired
    public ApiClient(ApiService apiService) {
        this.apiService = apiService;
    }

    //
    // ASYNCHRONOUS
    //

    public void getClients(final Callback<List<Client>> callback) {
        apiService.getRetrofit().fetchClients().enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable throwable) {
                callback.onFailure(call, throwable);
            }
        });
    }

    public void getPacketsByDate(String idClient, String date, final Callback<List<Packet>> callback) {
        apiService.getRetrofit().fetchPacketsByDate(idClient, date).enqueue(new Callback<List<Packet>>() {
            @Override
            public void onResponse(Call<List<Packet>> call, Response<List<Packet>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<List<Packet>> call, Throwable throwable) {
                callback.onFailure(call, throwable);
            }
        });
    }

    //
    //  SYNCHRONOUS
    //

    public List<Client> syncGetClient () throws IOException {
        return apiService.getRetrofit().fetchClients().execute().body();
    }

    public List<Packet> syncGetPacketsForDay (String idClient, String date) throws  IOException {
        return apiService.getRetrofit().fetchPacketsByDate(idClient, date).execute().body();
    }

    public List<PacketTemperature> syncGetPacketTemperature (String dateFrom, String dateTo) throws IOException {
        return apiService.getRetrofit().fetchTemperaturePacketsByDate(dateFrom, dateTo).execute().body();
    }

    public List<PacketHumidity> syncGetPacketHumidity(String dateFrom, String dateTo) throws IOException{
        return apiService.getRetrofit().fetchHumidityPacketsByDate(dateFrom, dateTo).execute().body();
    }

    public List<PacketRain> syncGetPacketRain(String dateFrom, String dateTo) throws IOException{
        return apiService.getRetrofit().fetchRainPacketsByDate(dateFrom, dateTo).execute().body();
    }

    public List<MapPacket> syncGetMapPacketsByTimestamp (String clientId, String dateTo, String dateFrom) throws IOException{
        return apiService.getRetrofit().fetchMapPacketsByTimeStamp(clientId, dateFrom, dateTo).execute().body();
    }
}
