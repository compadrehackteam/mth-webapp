package network.apiclient;


import network.domain.*;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.Date;
import java.util.List;

/**
 * This class implements the retrofit calls to the server. The configuration is implemented in
 * ApiService.java.
 */
public interface Interface {

    /**
     * Get the list of beacons
     */
    @GET("/getClients")
    Call<List<Client>> fetchClients();

    @GET("/getPacketsByDate/{clientId}/{date}")
    Call<List<Packet>> fetchPacketsByDate(@Path("clientId") String clientId, @Path("date") String date);

    @GET("/getForecast/temperature/{dateFrom}/{dateTo}")
    Call<List<PacketTemperature>> fetchTemperaturePacketsByDate(@Path("dateFrom") String dateFrom, @Path("dateTo") String dateTo);

    @GET("/getForecast/humidity/{dateFrom}/{dateTo}")
    Call<List<PacketHumidity>> fetchHumidityPacketsByDate(@Path("dateFrom") String dateFrom, @Path("dateTo") String dateTo);

    @GET("/getForecast/rain/{dateFrom}/{dateTo}")
    Call<List<PacketRain>> fetchRainPacketsByDate(@Path("dateFrom") String dateFrom, @Path("dateTo") String dateTo);

    @GET("/getMacsByDate/{clientId}/{dateTo}/{dateFrom}")
    Call<List<MapPacket>> fetchMapPacketsByTimeStamp (@Path("clientId") String clientId, @Path("dateTo") String dateTo, @Path("dateFrom") String dateFrom);
}

