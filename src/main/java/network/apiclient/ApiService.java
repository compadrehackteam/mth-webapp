package network.apiclient;

import com.google.gson.*;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ricardo on 11/09/15.
 *
 * <p>This ApiService class works with the configuration of auth and some tricks to parse date in
 * millis received from server to Java date object.
 */
public class ApiService {

    private static final String DEFAULT_BASE_URL = "http://ec2-18-220-192-200.us-east-2.compute.amazonaws.com:8585/api/";
//    private static final String DEFAULT_BASE_URL = "http://localhost:8081/";

    private String baseUrl;


    private OkHttpClient mOkHttpClient;

    private Interface retrofit;

////    public ApiService() {
////        this("", DEFAULT_BASE_URL);
////    }
//
//    public ApiService(String credentials) {
//        this(credentials);
//    }

    public ApiService() {
        this.baseUrl = DEFAULT_BASE_URL;
        this.mOkHttpClient = buildHttpClient();
        this.retrofit = buildAuthClient();
    }


    public Interface getRetrofit(){
        return retrofit;
    }


    private Interface buildAuthClient() {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(mOkHttpClient)
                .addConverterFactory(buildGsonConverter())
                .build().create(Interface.class);
    }

    private Interceptor buildOkInterceptor() {

        return chain -> {
            Request request;
            request = chain.request().newBuilder().build();
            return chain.proceed(request);
        };
    }

    private OkHttpClient buildHttpClient (){
        return new OkHttpClient.Builder()
                .addInterceptor(buildOkInterceptor())
                .build();
    }

    private GsonConverterFactory buildGsonConverter(){
        return GsonConverterFactory.create(buildGsonBuilder());
    }

    private Gson buildGsonBuilder(){
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> {
                    try {
                        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
                        Date date = format.parse(json.getAsString());
                        return date;
                    } catch (Exception e){
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                })
                .create();
    }
}
