package network.domain;

/**
 * Created by pedro on 31/08/17.
 */
public class DummyConcert {
    String fhour;
    int concert;

    public DummyConcert(String fHour, int concert) {
        this.fhour = fHour;
        this.concert = concert;
    }
}
